/**
 * Created by ascainiao on 2014/11/22.
 * 邮箱405076856@qq.com
 * CreateJS中文网 https://www.createjs.cc
 * QQ交流群：320648191
 * 
 */
import createjs from "createjs-ts";
import cjs=createjs
import DoubleFingerScaleHelper from "./helper/DoubleFingerScaleHelper";
import DragUtil from "./helper/DragUtil";
import Global from "./Global";
export default class Main extends cjs.Container {
    //private grid_shape:cjs.Shape=new cjs.Shape();
    private bitmap:cjs.Bitmap=new cjs.Bitmap('./assets/images/img.jpg');
    //
    private p0:cjs.Shape=new cjs.Shape();
    private p1:cjs.Shape=new cjs.Shape();
    private p2:cjs.Shape=new cjs.Shape();
    public constructor() {
        super();
        const box=new cjs.Container()
        box.setTransform(100,100)
        this.addChild(box);

        this.bitmap.setTransform(100,100,0.3,0.3,0,0,0,0,0);
        box.addChild(this.bitmap);
        //
        this.p0.graphics.clear();
        this.p0.graphics.beginFill('#ff0000').drawCircle(0,0,10);
        this.p0.graphics.endFill();
        this.addChild(this.p0);
        this.p1.graphics.clear();
        this.p1.graphics.beginFill('#ff0000').drawCircle(0,0,10);
        this.p1.graphics.endFill();
        this.addChild(this.p1);
        this.p2.graphics.clear();
        this.p2.graphics.beginFill('#00ff00').drawCircle(0,0,10);
        this.p2.graphics.endFill();
        this.addChild(this.p2);
        //
        this.on('added',(e:any)=>{
            this.init()
        },null,true)
    }
    private init():void{
        //
        new DoubleFingerScaleHelper(this.bitmap,Global.instance.ratio)
        new DragUtil(this.bitmap,()=>{
            //
        },()=>{
            //
        },()=>{
            //
        })
    }
    private down(p0:cjs.Point,p1:cjs.Point):void{
        //console.log('down')
        this.p0.x=p0.x
        this.p0.y=p0.y
        this.p1.x=p1.x
        this.p1.y=p1.y
        this.p2.x=(p0.x+p1.x)/2
        this.p2.y=(p0.y+p1.y)/2
    }
    private setPoint(p0:cjs.Point,p1:cjs.Point):void{
        this.p0.x=p0.x
        this.p0.y=p0.y
        this.p1.x=p1.x
        this.p1.y=p1.y
        this.p2.x=(p0.x+p1.x)/2
        this.p2.y=(p0.y+p1.y)/2
    }
}