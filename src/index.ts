/**
 * Created by ascainiao on 2014/11/22.
 * 邮箱405076856@qq.com
 * CreateJS中文网 https://www.createjs.cc
 * QQ交流群：320648191
 * 
 */
import { stageInit } from "./CreateJSHelper";
import Global from "./Global";
import Main from "./Main";
const canvas=document.getElementById('GameCanvas') as HTMLCanvasElement
//GameControl.getInstance().init(canvas)
const onresize=(e?:Event)=>{
    //console.log(window.innerWidth,window.innerHeight)
    Global.instance.ratio=750/canvas.clientWidth
    canvas.width=750
    canvas.height=canvas.clientHeight*Global.instance.ratio
    /*canvas.width=750/2
    canvas.height=1334/2*/
}
window.addEventListener('resize',onresize)
onresize()
const stage=stageInit(canvas,60,true,false)
const main=new Main()
stage.addChild(main)
const splash=document.getElementById('splash') as HTMLDivElement
splash.style.display='none'