/**
 * Created by ascainiao on 2014/11/22.
 * 邮箱405076856@qq.com
 * CreateJS中文网 https://www.createjs.cc
 * QQ交流群：320648191
 * 
 */
import createjs from 'createjs-ts';
import cjs=createjs
import Global from '@/Global';
type DragHandle=(target?: cjs.DisplayObject)=>void
export default class DragUtil {
    /*private target: cjs.DisplayObject;
    private startDrag: Function|((target?: cjs.DisplayObject)=>void);
    private dragging: Function|((target?: cjs.DisplayObject)=>void);
    private endDrag: Function|((target?: cjs.DisplayObject)=>void);*/
    constructor(target: cjs.DisplayObject,startDrag?:DragHandle,dragging?:DragHandle,endDrag?:DragHandle) {
        /*this.target = target;
        this.startDrag = startDrag;
        this.dragging = dragging;
        this.endDrag = endDrag;*/

        //let dx:number,dy:number
        const mouseDownHandle=target.stage.on("stagemousedown",(e:cjs.MouseEvent)=>{
            //console.log(e)
            const evt=e.nativeEvent
            //@ts-ignore
            console.log(evt.touches[0].clientX*Global.instance.ratio,evt.touches[0].clientY*Global.instance.ratio)
            console.log(e.stageX,e.stageY)
            //console.log('mousedown')
            const g=target.parent.localToGlobal(target.x,target.y)
            const dx=g.x-e.stageX
            const dy=g.y-e.stageY
            startDrag&&startDrag(target);
            const stageMouseMoveHandle=target.stage.on('stagemousemove',(e:any)=>{
                const evt=e.nativeEvent as TouchEvent
                if(evt.touches.length>1){return}
                //console.log('stagepressmove')
                const gx=e.stageX+dx
                const gy=e.stageY+dy

                target.parent.globalToLocal(gx, gy,target)
                dragging&&dragging(target);
            })
            const stageMouseUpHandle=target.stage.on('stagemouseup',()=>{
                target.stage.off('stagemousemove',stageMouseMoveHandle)
                target.stage.off('stagemouseup',stageMouseUpHandle)
                //target.off('mousedown',mouseDownHandle)
                endDrag&&endDrag(target);
            })
        });
    }
}