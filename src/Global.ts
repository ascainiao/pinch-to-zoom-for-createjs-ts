/**
 * Created by ascainiao on 2014/11/22.
 * 邮箱405076856@qq.com
 * CreateJS中文网 https://www.createjs.cc
 * QQ交流群：320648191
 * 
 */
export default class Global{
    private static _instance:Global;
    static get instance():Global{
        if(!this._instance){
            this._instance=new Global()
        }
        return this._instance
    }
    constructor(){
        console.log('Single Global')
    }
    //像素比
    ratio:number=1;
}