/**
 * Created by ascainiao on 2014/11/22.
 * 邮箱405076856@qq.com
 * CreateJS中文网 https://www.createjs.cc
 * QQ交流群：320648191
 * 
 */
import createjs from "createjs-ts";
import cjs=createjs;
/**
 * 初始化舞台
 * @param {*} canvas 
 * @param {Number} FPS 
 * @returns {cjs.Stage}
 */
const stageInit=(canvas:HTMLCanvasElement,FPS=60,touch=true,enableMouseOver=false,update?:any):cjs.Stage=>{
    const stage = new cjs.Stage(canvas)
    if(enableMouseOver)stage.enableMouseOver()
    if(touch)cjs.Touch.enable(stage)
    cjs.Ticker.framerate=FPS
    cjs.Ticker.addEventListener("tick", ()=> {
        stage.update()
        update&&update()
    })
    return stage
}
/**
 * 加载资源助手方法
 * @param assets 加载资源列表
 * @param basePath 资源基础路径
 * @param queue 加载器，使用该加载器加载资源，为null则new一个。
 * @param fileload 单个资源成功下载
 * @param progress 下载进度
 * @param plugin 资源类型
 * @returns 
 */
const loadManifestAsync=(assets:any[],basePath?: string,queue?:cjs.LoadQueue|null,fileload?:(e:cjs.Event)=>void,progress?:(e:cjs.Event)=>void,plugin?:any):Promise<cjs.LoadQueue>=>{
    return new Promise<cjs.LoadQueue>((resolve, reject)=>{
        const loader = queue||new cjs.LoadQueue(false);
        loader.addEventListener("complete", ()=>{resolve(loader)});
        if(fileload)
        loader.addEventListener('fileload', fileload);
        loader.addEventListener('error',(e:cjs.ErrorEvent):void=>{
            //loader.cancel()
            reject(e)
        })
        if(progress)
        loader.addEventListener('progress',progress)
        if(plugin)
        loader.installPlugin(plugin)
        loader.loadManifest(assets,true,basePath)
    })
}

export {stageInit,loadManifestAsync}