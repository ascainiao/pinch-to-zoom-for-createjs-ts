const { mode } = require("./webpack.config.prod");
module.exports={
    mode:'development',
    devtool: 'source-map',
    devServer:{
        open:true,
        host:'0.0.0.0',
        allowedHosts: "all"
    }
}