const path=require('path')
const HtmlWebpackPlugin=require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports={
    entry:'./src/index.ts',
    output:{
        path:path.resolve(__dirname,'../dist'),
        filename:'assets/js/bundle.js'
    },
    module:{
        rules:[
            {
                test:/\.ts$/,
                use:'ts-loader',
                exclude:/node-modules/
            }
        ]
    },
    resolve:{
        alias:{
            //在webpack中设置代码@符号表示src这一层目录
            '@':path.join(__dirname,'../src/')
        },
        extensions:['.ts','.js']
    },
    plugins:[
        //清除dist目录
        new CleanWebpackPlugin(),
        //生成html
        new HtmlWebpackPlugin({
            title:'双指缩放',
            template: 'template/template.html'
        }),
        //复制public目录下的文件到dist目录下
        new CopyWebpackPlugin({
            patterns:[
                {from:'public',to:'assets'},
                /*{
                    from:'fla',
                    to:'assets',
                    globOptions:{
                        gitignore: true,
                        ignore:['*.html','*.fla']
                    }
                }*/
            ]
        })
    ]
}