# Pinch-to-zoom-for-createjs-ts

#### 介绍
TypeScript实现CreateJS双指缩放

#### 使用说明

1.  安装依赖npm install
2.  调试运行npm run serve 
3.  发布npm run build

# CreateJS官网
[createjs官网](https://createjs.com/)

[createjs官网中文](https://createjs.cc/)

[createjs官网中文](https://createjs.site/)
# 社区交流
QQ群：320648191