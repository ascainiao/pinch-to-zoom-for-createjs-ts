const path=require('path')
const {merge}=require('webpack-merge')
const commonConfig=require('./config/webpack.config.common')
const devConfig=require('./config/webpack.config.dev')
const prodConfig=require('./config/webpack.config.prod')
module.exports=(env)=>{
    if(env&&env.production){
        return merge(commonConfig,prodConfig)
    }else{
        return merge(commonConfig,devConfig)
    }
}